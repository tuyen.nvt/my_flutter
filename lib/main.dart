import 'package:flutter/material.dart';
import 'dart:ui';

void main() => runApp(chooseWidget(window.defaultRouteName));

Widget chooseWidget(String route) {
  switch (route) {
    // name of the route defined in the host app
    case 'splashRoute':
      return MyFlutterActivity();

    default:
      return Scaffold(
        body: const Center(
          child: Text('Unknown'),
        ),
      );
  }
}

class MyFlutterActivity extends StatelessWidget {
  const MyFlutterActivity({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Flutter"),
        ),
      ),
    );
  }
}
